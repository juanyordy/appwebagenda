create database dbtramite;
go
use dbtramite
go
create table tusuario
(
codigoUsuario char(36) not null,
nombre varchar(70) not null,
apellido varchar(40) not null,
dniRuc char(12) not null,
fechaNacimiento datetime  null,
sexo bit  null,
correoElectronico varchar(700) not null,
contrasenia text not null,
extensionAvatar varchar(7) not null,
codigoRecuperacionContrasenia varchar(36) not null,
fechaCaducaCodigoRecuperacionContrasenia datetime not null,
motivoSuspensionBloqueo text not null,
rol text not null,
estado varchar(40) not null,
fechaRegistro datetime not null,
fechaModificacion datetime not null,
primary key(codigoUsuario)
);
go
create table ttipotramite
(
	codigoTipoTramite char(36) not null,
	nombre varchar(50) not null,
	descripcion varchar(700),
	fechaRegistro datetime not null,
	fechaModificacion datetime not null,
	primary key (codigoTipoTramite)
)
go
create table tarea
(
	codigoArea char(36) not null,
	nombre varchar(700) not null,
	descripcion varchar(700) not null,
	fechaRegistro datetime not null,
	fechaModificacion datetime not null,
	primary key (codigoArea)
);
go

create table tdocumento
(
 codigoDocumento char(36) not null,
 codigoTipoTramite char(36) not null,
 numeroExpediente int not null,
 nombre varchar(700) not null,
 folio int not null,
 tipo varchar(70) not null,
 asunto varchar(700) not null,
 extension varchar(10) not null,
 fechaRegistro datetime not null,
 fechaModificacion datetime not null,
 primary key (codigoDocumento),
 foreign key(codigoTipoTramite)references ttipotramite(codigoTipoTramite)
)
go
create table totdevtasignacion
(
	codigoToTDEvtAsignacion char(36) not null,
	codigoToTDEvtAsignacionPadre char(36) null,
	codigoDocumento char(36) not null,
	codigoArea char(36) not null,
	codigoUsuarioTrae char(36) not null,
	codigoUsuarioRecibe char(36) not null,
	activo bit not null,
	prioridad varchar(40) not null,
	estado varchar(70),
	rutaexpediente varchar(700) null,
	fechaDerivacion datetime null,
	fechaRegistro datetime not null,
	fechaModificacion datetime not null,
	primary key (codigoToTDEvtAsignacion),
	foreign key (codigoDocumento)references tdocumento(codigoDocumento),
	foreign key (codigoArea) references tarea(codigoArea),
	foreign key (codigoUsuarioTrae) references tusuario(codigoUsuario),
	foreign key (codigoUsuarioRecibe)references tusuario(codigoUsuario),
	foreign key (codigoToTDEvtAsignacionPadre) references totdevtasignacion(codigoToTDEvtAsignacion)
)
go

create table totdevtaarchivo
 (
 	codigoToTDEvtaArchivo char(36),
 	codigoToTDEvtAsignacion char(36) not null,
 	codigoDocumento char(36) not null,
 	fechaRegistro datetime not null,
 	fechaModificacion datetime not null,
 	primary key (codigoToTDEvtaArchivo),
 	foreign key (codigoDocumento)references tdocumento(codigoDocumento),
 	foreign key (codigoToTDEvtAsignacion)references totdevtasignacion(codigoToTDEvtAsignacion)
 );
 go