create database dbagenda;
use dbagenda;

create table tusuario
(
codigoUsuario char(13) not null,
nombre varchar(70) not null,
apellido varchar(40) not null,
dni char(8) not null,
fechaNacimiento date not null,
sexo bool not null,
correoElectronico varchar(700) not null,
contrasenia text not null,
rol varchar(700) not null,
extensionAvatar varchar(7) not null,
created_at datetime not null,
updated_at datetime not null,
primary key(codigoUsuario)
);

create table tpublicacion
(
codigoPublicacion char(13) not null,
codigoUsuario char(13) not null,
descripcion text not null,
created_at datetime not null,
updated_at datetime not null,
foreign key(codigoUsuario) references tusuario(codigoUsuario) on delete cascade on update cascade,
primary key(codigoPublicacion)
);

create table tpublicaciontusuarioevtetiqueta
(
codigoPublicacionEvtEtiqueta char(13) not null,
codigoPublicacion char(13) not null,
codigoUsuario char(13) not null,
created_at datetime not null,
updated_at datetime not null,
foreign key(codigoPublicacion) references tpublicacion(codigoPublicacion) on delete cascade on update cascade,
foreign key(codigoUsuario) references tusuario(codigoUsuario) on delete cascade on update cascade,
primary key(codigoPublicacionEvtEtiqueta)
);

create table tpublicaciontusuarioevtreaccion
(
codigoPublicacionTUsuarioEvtReaccion char(13) not null,
codigoPublicacion char(13) not null,
codigoUsuario char(13) not null,
gusta bool not null,
created_at datetime not null,
updated_at datetime not null,
foreign key(codigoPublicacion) references tpublicacion(codigoPublicacion) on delete cascade on update cascade,
foreign key(codigoUsuario) references tusuario(codigoUsuario) on delete cascade on update cascade,
primary key(codigoPublicacionTUsuarioEvtReaccion)
);

create table tpublicacioncomentario
(
codigoPublicacionComentario char(13) not null,
codigoPublicacion char(13) not null,
codigoUsuario char(13) not null,
codigoPublicacionComentarioPadre char(13) null,
descripcion text not null,
created_at datetime not null,
updated_at datetime not null,
foreign key(codigoPublicacion) references tpublicacion(codigoPublicacion) on delete cascade on update cascade,
foreign key(codigoUsuario) references tusuario(codigoUsuario) on delete cascade on update cascade,
foreign key(codigoPublicacionComentarioPadre) references tpublicacioncomentario(codigoPublicacionComentario) on delete cascade on update cascade,
primary key(codigoPublicacionComentario)
);