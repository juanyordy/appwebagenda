<?php
//INDEX
Route::get('/', 'IndexController@actionIndex');

//USUARIO
Route::match(['get', 'post'], 'usuario/insertar', 'UsuarioController@actionInsertar');
Route::match(['get', 'post'], 'usuario/login', 'UsuarioController@actionLogIn');
Route::match(['get', 'post'], 'usuario/editar', 'UsuarioController@actionEditar');
Route::get('usuario/logout', 'UsuarioController@actionLogOut');
Route::post('usuario/cambiaravatar', 'UsuarioController@actionCambiarAvatar');

//PUBLICACION
Route::get('publicacion/index/{codigoPublicacion?}', 'PublicacionController@actionIndex');
Route::post('publicacion/index', 'PublicacionController@actionIndex');
Route::get('publicacion/eliminar/{codigoPublicacion}','PublicacionController@actionEliminar');
Route::post('publicacion/vermenorfechacreacion', 'PublicacionController@actionVerMenorFechaCreacion');

//PUBLICACIONTUSUARIOEVTREACCION
Route::post('publicaciontusuarioevtreaccion/insertar', 'PublicacionTUsuarioEvtReaccionController@actionInsertar');

//notificacion
Route::post('notificacion/nueva','PublicacionController@actionNotificacion');

//PublicacionComentario
Route::match(['get','post'],'publicacion/comentario/insertar','PublicacionComentarioController@actionInsertar');

// publicacionTusuarioEvtEtgiqueta
Route::match(['get','post'],'publicacion/etiqueta/insertar','PublicacionTUsuarioEvtEtiquetaController@actionInsertar');
Route::post('publicacion/etiqueta/insertar1','PublicacionTUsuarioEvtEtiquetaController@actionInsertar1');
//pdf
Route::get('pdf','PdfController@actionIsidencias');
//exel
Route::get('exel','ExelController@actionExel');
Route::get('exelPublicaciones','ExelController@actionExelPublicaciones');
	
?>