<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TNotificacion extends Model
{
	protected $table='tnotificacion';
	protected $primaryKey='codigoNotificacion';
	public $incrementing=false;
	public $timestamps=true;
}
?>