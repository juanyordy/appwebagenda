<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TPublicacion extends Model
{
	protected $table='tpublicacion';
	protected $primaryKey='codigoPublicacion';
	public $incrementing=false;
	public $timestamps=true;

	public function tUsuario()
	{
		return $this->belongsTo('App\Model\TUsuario', 'codigoUsuario');
	}

	public function tPublicacionTUsuarioEvtEtiqueta()
	{
		return $this->hasMany('App\Model\TPublicacionTUsuarioEvtEtiqueta', 'codigoPublicacion');
	}

	public function tPublicacionTUsuarioEvtReaccion()
	{
		return $this->hasMany('App\Model\TPublicacionTUsuarioEvtReaccion', 'codigoPublicacion');
	}

	public function tPublicacionComentario()
	{
		return $this->hasMany('App\Model\TPublicacionComentario', 'codigoPublicacion');
	}
}
?>