<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TUsuario extends Model
{
	protected $table='tusuario';
	protected $primaryKey='codigoUsuario';
	public $incrementing=false;
	public $timestamps=true;

	public function tPublicacion()
	{
		return $this->hasMany('App\Model\TPublicacion', 'codigoUsuario');
	}

	public function tPublicacionTUsuarioEvtEtiqueta()
	{
		return $this->hasMany('App\Model\TPublicacionTUsuarioEvtEtiqueta', 'codigoUsuario');
	}

	public function tPublicacionTUsuarioEvtReaccion()
	{
		return $this->hasMany('App\Model\TPublicacionTUsuarioEvtReaccion', 'codigoUsuario');
	}

	public function tPublicacionComentario()
	{
		return $this->hasMany('App\Model\TPublicacionComentario', 'codigoUsuario');
	}
}
?>