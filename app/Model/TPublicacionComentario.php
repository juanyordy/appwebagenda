<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TPublicacionComentario extends Model
{
	protected $table='tpublicacioncomentario';
	protected $primaryKey='codigoPublicacionComentario';
	public $incrementing=false;
	public $timestamps=true;

	public function tPublicacion()
	{
		return $this->belongsTo('App\Model\TPublicacion', 'codigoPublicacion');
	}

	public function tUsuario()
	{
		return $this->belongsTo('App\Model\TUsuario', 'codigoUsuario');
	}

	public function tPublicacionComentarioPadre()
	{
		return $this->belongsTo('App\Model\TPublicacionComentario', 'codigoPublicacionComentario');
	}

	public function tPublicacionComentarioHijo()
	{
		return $this->hasMany('App\Model\TPublicacionComentario', 'codigoPublicacionComentarioPadre');
	}
}
?>