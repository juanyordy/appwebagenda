<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TPublicacionTUsuarioEvtEtiqueta extends Model
{
	protected $table='tpublicaciontusuarioevtetiqueta';
	protected $primaryKey='codigoPublicacionEvtEtiqueta';
	public $incrementing=false;
	public $timestamps=true;

	public function tPublicacion()
	{
		return $this->belongsTo('App\Model\TPublicacion', 'codigoPublicacion');
	}

	public function tUsuario()
	{
		return $this->belongsTo('App\Model\TUsuario', 'codigoUsuario');
	}
}
?>