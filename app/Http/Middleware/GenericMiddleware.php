<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Model\TNotificacion;
class GenericMiddleware
{
    public function handle($request, Closure $next)
    {
       // Session::flush();
        //exit;
        $urlRequest=$request->url();
        $urlBase=env('URL_BASE_FILTER');

        //Session::flush();
        $filterRol=explode(',', Session::get('rol', 'Anónimo'));

        $urlAccess=
        [
            [$urlBase, 'Súper usuario,Usuario normal',false],
            
            [$urlBase.'/usuario/insertar', 'Súper usuario,Usuario normal,Anónimo',false],
            [$urlBase.'/usuario/login', 'Súper usuario,Usuario normal,Anónimo',false],
            [$urlBase.'/usuario/editar', 'Súper usuario,Usuario normal',false],
            [$urlBase.'/usuario/logout', 'Súper usuario,Usuario normal',false],
            [$urlBase.'/usuario/cambiaravatar', 'Súper usuario,Usuario normal',false],

            [$urlBase.'/publicacion/index', 'Súper usuario,Usuario normal',true],
            [$urlBase.'/publicacion/eliminar','Súper usuario,Usuario normal',true],
            [$urlBase.'/publicaciontusuarioevtreaccion/insertar', 'Súper usuario,Usuario normal',false],
            [$urlBase.'/publicacion/vermenorfechacreacion', 'Súper usuario,Usuario normal', false],
            [$urlBase.'/notificacion/nueva', 'Súper usuario,Usuario normal', false],
            [$urlBase.'/pdf', 'Súper usuario,Usuario normal', false],
            [$urlBase.'/exel', 'Súper usuario,Usuario normal', false],
            [$urlBase.'/exelPublicaciones', 'Súper usuario,Usuario normal', false],
            [$urlBase.'/publicacion/comentario/insertar', 'Súper usuario,Usuario normal', false],
            [$urlBase.'/publicacion/etiqueta/insertar', 'Súper usuario,Usuario normal', false],
             [$urlBase.'/publicacion/etiqueta/insertar1', 'Súper usuario,Usuario normal', false]


            
        ];

        $access=false;

        foreach($urlAccess as $key => $value)
        {
            if($value[0]==$urlRequest||($value[2] && strpos($urlRequest, $value[0])===0))
            {
                foreach($filterRol as $index => $item)
                {
                    if(strpos($value[1], $item)!==false)
                    {
                        $access=true;

                        break 2;
                    }
                }
            }
        }

        if(!$access)
        {
            if($request->ajax())
            {
                exit;
            }

            return redirect('usuario/login');
        }
        $cantidadNotificacion=TNotificacion::count();

        view()->share('cantidadNotificacion',$cantidadNotificacion);

        return $next($request);
    }
}
?>