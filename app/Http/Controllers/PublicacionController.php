<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Validator;
use Mail;
use App\Model\TPublicacion;
use App\Model\TUsuario;
use App\Model\TNotificacion;

class PublicacionController extends Controller
{
	public function actionIndex(Request $request, SessionManager $sessionManager,$codigoPublicacion=null)
	{
		if($_POST)
		{
			$validator=Validator::make(
			[
				'Descripción' => trim($request->input('txtDesripcion'))
			],
			[
				'Descripción' => ['required']
			],
			[
				'required' => 'Debe ingresar algo para realizar su publicación.\n'
			]);

			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($mensajeGlobal!='')
			{
				$request->flash();

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', false);
				
				return redirect('/publicacion/index');
			}

			$tPublicacion=new TPublicacion();

			$codigoPublicacion=$tPublicacion->codigoPublicacion=uniqid();
			$tPublicacion->codigoUsuario=$sessionManager->get('codigoUsuario');
			$tPublicacion->descripcion=$request->input('txtDesripcion');

			$tPublicacion->save();

			$tNotificacion=new TNotificacion();

			$tNotificacion->codigoNotificacion=uniqid();
			$tNotificacion->descripcion= 'El usuario '.$sessionManager->get('nombre').' '.$sessionManager->get('apellido'). ' realizo una publicacion de '.$tPublicacion->descripcion=$request->input('txtDesripcion');
			$tNotificacion->url='http://localhost:81/appwebagend/public/publicacion/index/'.$codigoPublicacion;

			$tNotificacion->save();


			$listaTUsuario=TUsuario::whereRaw('codigoUsuario!=?', [$sessionManager->get('codigoUsuario')])->get();

			
			return redirect('/publicacion/index');
		}

		$listaTPublicacion=TPublicacion::with(['tusuario', 'tpublicaciontusuarioevtreaccion','tpublicaciontusuarioevtetiqueta.tusuario','tpublicacioncomentario.tusuario'])->orderBy('created_at','desc')->take(7)->get();
		$listaTNotificacion=TNotificacion::orderBy('created_at','desc')->take(7)->get();

		foreach($listaTPublicacion as $key => $value)
		{
			$meGusta=0;
			$noMeGusta=0;

			foreach($value->tpublicaciontusuarioevtreaccion as $index => $item)
			{
				($item->gusta ? $meGusta++ : $noMeGusta++);
			}

			$value->meGusta=$meGusta;
			$value->noMeGusta=$noMeGusta;
		}

		$tPublicacion=TPublicacion::find($codigoPublicacion);


		return view('publicacion/index', ['tPublicacion'=>$tPublicacion,'listaTPublicacion' => $listaTPublicacion,'listaTPublicacion'=>$listaTPublicacion]);
	}
	public function actionEliminar($codigoPublicacion,SessionManager $sessionManager)
	{	
		$codigoSeccion=$sessionManager->get('codigoUsuario');
		$tPublicacion=TPublicacion::find($codigoPublicacion);

		if(($codigoSeccion==$tPublicacion->codigoUsuario)&&($codigoPublicacion==$tPublicacion->codigoPublicacion))
		{
			$tPublicacion=TPublicacion::find($codigoPublicacion);
		
			$tPublicacion->delete();	

			$sessionManager->flash('mensajeGlobal', 'Publicacion Eliminada.');
			$sessionManager->flash('correcto', true);

			return redirect('publicacion/index');
		}
		else
		{
			$sessionManager->flash('mensajeGlobal', 'Usted no esta autorizado para eliminar Esta publicacion (:');
			$sessionManager->flash('correcto', false);
			return redirect('publicacion/index');
		}		

	}
	function actionVerMenorFechaCreacion(Request $request)
	{
		$listaTPublicacion=TPublicacion::with(['tusuario', 'tpublicaciontusuarioevtreaccion'])->whereRaw('created_at<?', [$request->input('createdAt')])->orderBy('created_at', 'desc')->take(7)->get();

		foreach($listaTPublicacion as $key => $value)
		{
			$meGusta=0;
			$noMeGusta=0;

			foreach($value->tpublicaciontusuarioevtreaccion as $index => $item)
			{
				($item->gusta ? $meGusta++ : $noMeGusta++);
			}

			$value->meGusta=$meGusta;
			$value->noMeGusta=$noMeGusta;
		}

		return view('publicacion/vermenorfechacreacion', ['listaTPublicacion' => $listaTPublicacion]);
	}

	public function actionNotificacion(Request $request)
	{
		$listaTNotificacion=TNotificacion::all();

		$arrayNotificacion=[];

		foreach($listaTNotificacion as $value)
		{
				$arrayNotificacion[]=['url' => $value->url, 'descripcion' => $value->descripcion];
		}

		return response()->json($arrayNotificacion);

	}

}
?>