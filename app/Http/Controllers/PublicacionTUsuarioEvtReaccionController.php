<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;

use Mail;

use App\Model\TPublicacionTUsuarioEvtReaccion;
use App\Model\TPublicacion;

class PublicacionTUsuarioEvtReaccionController extends Controller
{
	public function actionInsertar(Request $request, SessionManager $sessionManager)
	{
		$tPublicacion=TPublicacion::with('tusuario')->whereRaw('codigoPublicacion=?', [$request->input('codigoPublicacion')])->first();

		$tPublicacionTUsuarioEvtReaccion=TPublicacionTUsuarioEvtReaccion::whereRaw('codigoPublicacion=? and codigoUsuario=?', [$request->input('codigoPublicacion'), $sessionManager->get('codigoUsuario')])->first();

		if($tPublicacionTUsuarioEvtReaccion!=null)
		{
			if($tPublicacionTUsuarioEvtReaccion->gusta==$request->input('gusta'))
			{
				return response()->json(['correcto' => false, 'mensajeGlobal' => 'No se puede dar una reacción más de una vez sobre la misma publicación']);
			}
			else
			{
				$tPublicacionTUsuarioEvtReaccion->gusta=$request->input('gusta');

				$tPublicacionTUsuarioEvtReaccion->save();

				/*Mail::send('mail.likemail', ['mensaje' => 'Se ha ralizado una reacción en una de tus publicaciones.'], function($x) use($tPublicacion)
				{
					$x->from(env('MAIL_USERNAME'), 'Spring Valley Software');
					$x->to($tPublicacion->tusuario->correoElectronico, $tPublicacion->tusuario->nombre.' '.$tPublicacion->tusuario->apellido)->subject('Reacción en una publicación');
				});
				*/

				return response()->json(['correcto' => true, 'cambio' => true]);
			}
		}

		$tPublicacionTUsuarioEvtReaccion=new TPublicacionTUsuarioEvtReaccion();

		$tPublicacionTUsuarioEvtReaccion->codigoPublicacionTUsuarioEvtReaccion=uniqid();
		$tPublicacionTUsuarioEvtReaccion->codigoPublicacion=$request->input('codigoPublicacion');
		$tPublicacionTUsuarioEvtReaccion->codigoUsuario=$sessionManager->get('codigoUsuario');
		$tPublicacionTUsuarioEvtReaccion->gusta=$request->input('gusta');

		$tPublicacionTUsuarioEvtReaccion->save();

		/*Mail::send('mail.likemail', ['mensaje' => 'Se ha ralizado una reacción en una de tus publicaciones.'], function($x) use($tPublicacion)
		{
			$x->from(env('MAIL_USERNAME'), 'Spring Valley Software');
			$x->to($tPublicacion->tusuario->correoElectronico, $tPublicacion->tusuario->nombre.' '.$tPublicacion->tusuario->apellido)->subject('Reacción en una publicación');
		});
		*/

		return response()->json(['correcto' => true]);
	}
}
?>