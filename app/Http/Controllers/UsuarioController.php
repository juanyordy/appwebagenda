<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;

use App\Model\TUsuario;

class UsuarioController extends Controller
{
	public function actionInsertar(Request $request, Encrypter $encrypter, SessionManager $sessionManager)
	{
		if($_POST)
		{
			$validator=Validator::make(
			[
				'Nombre' => trim($request->input('txtNombre')),
				'Apellido' => trim($request->input('txtApellido')),
				'Correo electrónico' => trim($request->input('txtCorreoElectronico')),
				'Contraseña' => $request->input('passContrasenia')
			],
			[
				'Nombre' => ['required'],
				'Apellido' => ['required'],
				'Correo electrónico' => ['required', 'email', 'unique:tusuario,correoElectronico'],
				'Contraseña' => ['required']
			],
			[
				'required' => 'El campo ":Attribute" es requerido.<br>',
				'email' => 'El formato del campo ":Attribute" es incorrecto.<br>',
				'unique' => 'El ":Attribute" ya se encuentra registrado en el sistema.<br>'
			]);

			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($request->input('passContrasenia')!=$request->input('passContraseniaRepita'))
			{
				$mensajeGlobal.='Las contraseñas no coinciden.<br>';
			}

			if($mensajeGlobal!='')
			{
				$request->flash();

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', 'No');
				
				return redirect('/usuario/insert');
			}

			$codigoUsuarioTemp=uniqid();

			copy(public_path().'/avatar/user.png', public_path().'/avatar/'.$codigoUsuarioTemp.'.png');

			$tUsuario=new TUsuario();

			$tUsuario->codigoUsuario=$codigoUsuarioTemp;
			$tUsuario->nombre=trim($request->input('txtNombre'));
			$tUsuario->apellido=trim($request->input('txtApellido'));
			$tUsuario->correoElectronico=trim($request->input('txtCorreoElectronico'));
			$tUsuario->contrasenia=$encrypter->encrypt($request->input('passContrasenia'));
			$tUsuario->dni='';
			$tUsuario->fechaNacimiento='1991-01-01';
			$tUsuario->sexo=true;
			$tUsuario->extensionAvatar='png';
			$tUsuario->rol='Usuario normal';

			$tUsuario->save();

			$sessionManager->put('codigoUsuario', $codigoUsuarioTemp);
			$sessionManager->put('correoElectronico', trim($request->input('txtCorreoElectronico')));
			$sessionManager->put('nombre', trim($request->input('txtNombre')));
			$sessionManager->put('extensionAvatar', 'png');

			$sessionManager->flash('mensajeGlobal', 'Fuiste registrado(a) correctamente en el sistema.');
			$sessionManager->flash('correcto', 'Si');

			return redirect('/');
		}

		return view('usuario/insertar');
	}

	public function actionLogIn(Request $request, Encrypter $encrypter, SessionManager $sessionManager)
	{
		if($_POST)
		{
			$correoElectronico=$request->input('txtCorreoElectronico');
			$contrasenia=$request->input('passContrasenia');

			$tUsuario=TUsuario::where('correoElectronico', $correoElectronico)->first();

			if($tUsuario==null)
			{
				return redirect('usuario/login');
			}

			if($encrypter->decrypt($tUsuario->contrasenia)!=$contrasenia)
			{
				return redirect('usuario/login');
			}

			$sessionManager->put('codigoUsuario', $tUsuario->codigoUsuario);
			$sessionManager->put('correoElectronico', $tUsuario->correoElectronico);
			$sessionManager->put('rol', $tUsuario->rol);
			$sessionManager->put('nombre', $tUsuario->nombre);
			$sessionManager->put('extensionAvatar', $tUsuario->extensionAvatar);
			$sessionManager->put('apellido',$tUsuario->apellido);

			return redirect('/');
		}

		return view('usuario/login');
	}

	public function actionLogOut(SessionManager $sessionManager)
	{
		$sessionManager->flush();

		return redirect('/usuario/login');
	}

	public function actionEditar(Request $request, SessionManager $sessionManager)
	{
		if($_POST)
		{
			$validator=Validator::make(
			[
				'Nombre' => trim($request->input('txtNombre')),
				'Apellido' => trim($request->input('txtApellido')),
				'Correo electrónico' => trim($request->input('txtCorreoElectronico')),
				'Fecha de nacimiento' => trim($request->input('dateFechaNacimiento')),
				'Dni' => trim($request->input('txtDni'))
			],
			[
				'Nombre' => ['required'],
				'Apellido' => ['required'],
				'Correo electrónico' => ['required', 'email', 'unique:tusuario,correoElectronico,'.$sessionManager->get('codigoUsuario').',codigoUsuario'],
				'Fecha de nacimiento' => ['required', 'regex:/^((((1|2){1}\d{3}\/(0|1){1}\d{1}\/[0-3]{1}\d{1})|((1|2){1}\d{3}\-(0|1){1}\d{1}\-[0-3]{1}\d{1})){1})*$/'],
				'Dni' => ['regex:/^[0-9]{8}$/']
			],
			[
				'required' => 'El campo ":Attribute" es requerido.<br>',
				'email' => 'El formato del campo ":Attribute" es incorrecto.<br>',
				'unique' => 'El ":Attribute" ya se encuentra registrado en el sistema.<br>',
				'regex' => 'El formato del campo ":Attribute" es incorrecto.<br>',
			]);

			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($mensajeGlobal!='')
			{
				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', false);
				
				return redirect('/usuario/editar');
			}

			$tUsuario=TUsuario::find($sessionManager->get('codigoUsuario'));

			$tUsuario->nombre=trim($request->input('txtNombre'));
			$tUsuario->apellido=trim($request->input('txtApellido'));
			$tUsuario->correoElectronico=trim($request->input('txtCorreoElectronico'));
			$tUsuario->dni=trim($request->input('txtDni'));
			$tUsuario->fechaNacimiento=trim($request->input('dateFechaNacimiento'));
			$tUsuario->sexo=($request->input('radioSexo')=='M' ? true : false);

			$tUsuario->save();

			$sessionManager->flash('mensajeGlobal', 'Datos guardados correctamente.');
			$sessionManager->flash('correcto', true);

			return redirect('/usuario/editar');
		}

		$tUsuario=TUsuario::find($sessionManager->get('codigoUsuario'));

		return view('usuario/editar', ['tUsuario' => $tUsuario]);
	}

	public function actionCambiarAvatar(Request $request, SessionManager $sessionManager)
	{
		if($request->has('hdPost'))
		{
			if(!($request->hasFile('fileAvatar')))
			{
				$sessionManager->flash('mensajeGlobal', 'No se a seleccionado archivo.');
				$sessionManager->flash('correcto', false);

				return redirect('/usuario/editar');
			}

			$fileGetClientOriginalExtension=strtolower($request->file('fileAvatar')->getClientOriginalExtension());
			$fileGetSizeKb=(($request->file('fileAvatar')->getSize())/1024);
			
			if($fileGetClientOriginalExtension!='jpg' && $fileGetClientOriginalExtension!='jpeg' && $fileGetClientOriginalExtension!='png')
			{
				$sessionManager->flash('mensajeGlobal', 'El formato del archivo sólo puede ser jpg, jpeg o png.');
				$sessionManager->flash('correcto', false);

				return redirect('/usuario/editar');
			}

			if($fileGetSizeKb>3072)
			{
				$sessionManager->flash('mensajeGlobal', 'El tamaño del archivo no puede ser mayor a 3mb.');
				$sessionManager->flash('correcto', false);

				return redirect('/usuario/editar');
			}

			$tUsuario=TUsuario::find($sessionManager->get('codigoUsuario'));

			$rutaAvatarActual=public_path().'/avatar/'.$tUsuario->codigoUsuario.'.'.$tUsuario->extensionAvatar;

			$tUsuario->extensionAvatar=$fileGetClientOriginalExtension;

			if(file_exists($rutaAvatarActual))
			{
				unlink($rutaAvatarActual);
			}

			$request->file('fileAvatar')->move(public_path().'/avatar', $sessionManager->get('codigoUsuario').'.'.$fileGetClientOriginalExtension);

			$tUsuario->save();

			$sessionManager->put('extensionAvatar', $fileGetClientOriginalExtension);

			$sessionManager->flash('mensajeGlobal', 'Avatar actualizado correctamente.');
			$sessionManager->flash('correcto', true);

			return redirect('/usuario/editar');
		}

		return view('usuario/cambiaravatar');
	}
}
?>