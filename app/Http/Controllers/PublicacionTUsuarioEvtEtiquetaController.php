<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;

use App\Model\TPublicacionTUsuarioEvtEtiqueta;
use App\Model\TUsuario;

class PublicacionTUsuarioEvtEtiquetaController extends Controller
{
	public function actionInsertar(Request $request)
	{
		$tUsuarios=TUsuario::all();
		return view('publicaciontusuarioevtetiqueta/insertar',['tUsuarios'=>$tUsuarios,'codigoPublicacion'=>$request->input('codigoPublicacion')]);
	}
	public function actionInsertar1(Request $request)
	{
		$tPublicacionTUsuarioEvtEtiquetaExiste=TPublicacionTUsuarioEvtEtiqueta::whereRaw('codigoUsuario=? and codigoPublicacion=?',[$request->input('codigoUsuario'),$request->input('codigoPublicacion')])->first();

		if($tPublicacionTUsuarioEvtEtiquetaExiste!=null)
		{

			return response()->json(['correcto' => false, 'mensajeGlobal' => 'Usuario etiquetado anteriormente ']);
		}
		
			$tPublicacionTUsuarioEvtEtiqueta=new TPublicacionTUsuarioEvtEtiqueta();

			$tPublicacionTUsuarioEvtEtiqueta->codigoPublicacionEvtEtiqueta=uniqid();
			$tPublicacionTUsuarioEvtEtiqueta->codigoPublicacion=$request->input('codigoPublicacion');
			$tPublicacionTUsuarioEvtEtiqueta->codigoUsuario=$request->input('codigoUsuario');

			$tPublicacionTUsuarioEvtEtiqueta->save();

		return response()->json(['correcto' => true, 'mensajeGlobal' => 'Usuario Etiquetado']);

	}

}
	
?>	