<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Model\TPublicacion;
class PdfController extends Controller
{
	public function actionpdf(Application $aplication)
	{
		$usuariosGusta='';
		$tPublicacion=TPublicacion::with(['tusuario', 'tpublicaciontusuarioevtreaccion.tusuario'])->get();
		/*foreach ($tPublicacion as $value)
		{
			$value->publicacion
			foreach ($value as $key => $reaacion) 
			{
				$reaacion->gusta
			}
			if()
		}
		*/
		$pdf = $aplication->make('dompdf.wrapper');
		$pdf->loadHTML(View('pdf/pdf',['tpublicacion'=>$tPublicacion]));
		
		return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
	}
}