<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

use App\Model\TPublicacion;
class ExelController extends Controller
{
	public function actionExel(Application $aplication)
	{
		$excel=$aplication->make('excel');
			
			$excel->create('mi archivo de prueba',function($filaExcel)
			{
				$filaExcel->sheet('hoja uno ',function($sheet)
				{
					$culumnaA=['A','B','C','D','E','F','G'];
					$culumnaB=['G','F','E','D','C','B','A'];

					for ($i=0; $i <7 ; $i++) 
					{ 
						$sheet->cells($culumnaA[$i].($i+1),function($cell)use($i)
						{
							$cell->setValue($i);

						});
				

						$sheet->cells($culumnaB[$i].($i+1),function($cell)use($i)
						{
							$cell->setValue($i);

						});
					} 
					
				});

			})->download('xlsx');
	}
	public function actionExelPublicaciones(Application $aplication)
	{
		
		$excel=$aplication->make('excel');
		$excel->create('Lista Publicaciones',function($filaExcel)
		{
			$tPublicaciones=TPublicacion::with('tusuario')->get();
			$filaExcel->sheet('hoja',function($sheet)use($tPublicaciones)
			{
				foreach ($tPublicaciones as $value)
				{
					$sheet->fromArray(array(
					array($value->tusuario->nombre,$value->descripcion)));
				
				}
					}
				);

			})->download('xlsx');

		}
	
}
