<?php
namespace App\Http\Controllers;
	
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;

use App\Model\TPublicacionComentario;
use App\Model\TPublicacion;	
class PublicacionComentarioController extends Controller
{
	
	public function actionInsertar(SessionManager $sessionManager,Request $request)
	{	
		if($_POST)
		{
			if($request->has('hdPost'))
			{
				
				$tPublicacionComentario=new TPublicacionComentario();
	
				//dd($request->input('textCodigoPublicacion'));exit;
				
				$tPublicacionComentario->codigoPublicacionComentario=uniqid();
				$tPublicacionComentario->codigoPublicacion=$request->input('textCodigoPublicacion');
				$tPublicacionComentario->codigoUsuario=$sessionManager->get('codigoUsuario');
				$tPublicacionComentario->codigoPublicacionComentarioPadre=null;
				$tPublicacionComentario->descripcion=$request->input('textDescripcion');
	
				$tPublicacionComentario->save();

				$sessionManager->flash('mensajeGlobal','Comentatio Guardado');
				$sessionManager->flash('correcto',true);

				return redirect('/publicacion/index');
			}
			$tPublicacion=TPublicacion::find($request->input('codigoPublicacion'));
			//dd($tPublicacion);exit;
							
			return view('publicacioncomentario/insertar',['tPublicacion'=>$tPublicacion]);
		
		
		}
	}

		
}
?>		