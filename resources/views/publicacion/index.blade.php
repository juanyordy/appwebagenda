@extends('template.templategeneric')
@section('tituloGeneral', 'Publicación')
@section('subTituloGeneral', 'Muro de publicaciones')
@section('cuerpoGeneral')
<div class="col-md-12">
	<div class="col-md-5">
		<div class="box box-primary">
			<!--<div class="box-header with-border">
				<h3 class="box-title"></h3>
			</div>-->
			<form action="{{url('publicacion/index')}}" method="post">
				<div class="box-body">
					<textarea id="txtDesripcion" name="txtDesripcion" rows="3" class="form-control" style="resize: none;" placeholder="Publica algo para que todos lo vean"></textarea>
				</div>
				<div class="box-footer">
					<input type="submit" value="Realizar publicación" class="btn btn-info pull-right">
				</div>
				{{csrf_field()}}
			</form>
		</div>
	</div>
	<div id="divContenedorPublicaciones" class="col-md-7">
		@foreach($listaTPublicacion as $key => $value)
			
			<div class="box" style="margin-bottom: 4px;">
				<div class="box-header with-border">
          			<h3 class="box-title">
          				<img src="{{asset('avatar/'.$value->tusuario->codigoUsuario.'.'.$value->tusuario->extensionAvatar)}}" height="40" width="40" style="border: 1px solid #999999;border-radius: 50px;">
          				<input type="hidden" class="hdCreatedAt" value="{{$value->created_at}}">
						<b>{{$value->tusuario->nombre.' '.$value->tusuario->apellido}}</b>
						
						@if(Session::get('codigoUsuario')==$value->codigoUsuario)
							|<a href="#" class="btn-info btn-xs " onclick="etiquetarUsuario('{{$value->codigoPublicacion }}');">Etiquetar</a>
						@endif
						<a class="btn-success btn-xs divEtiquetadosUsuario" id="" href="#" data-toggle="tooltip" title="dfs">ver </a> 
						
						<div class="divEtiquetados">
							@foreach($value->tpublicaciontusuarioevtetiqueta as $susarioEtiqueta )
							<p style="color:red" data-toggle="tooltip" title="{{$susarioEtiqueta->tusuario->nombre }}">{{$susarioEtiqueta->tusuario->nombre }}</p>
							@endforeach
						</div>

						<div id="amigosetiquetados"></div>|
						<div id="etiqueta"></div>
          			</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
				<form action="{{url('publicacion/index')}}" method="post">
					<div class="box-body">
						<div class="col-md-12">
							<table style="font-family:sans-serif; ">
								<tbody>
									<tr>	
										<td>	
											{{$value->descripcion}}
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer" style="text-align: right;">
						@if(Session::get('codigoUsuario')==$value->codigoUsuario)
							<a href="{{url('publicacion/eliminar/'.$value->codigoPublicacion)}}" style="color: red;">Eliminar</a> | 
						@endif
						
						|<a href="#">Ver Comentarios</a>|<a href="#" onclick="publicacionComentario('{{ $value->codigoPublicacion }}');">Comentar</a>|	
						<span style="color: #999999;font-size: 12px;">{{$value->created_at}}</span> - <a href="#" onclick="enviarReaccion('{{$value->codigoPublicacion}}', 1, this);">Me gusta (<span>{{$value->meGusta}}</span>)</a> | <a href="#" onclick="enviarReaccion('{{$value->codigoPublicacion}}', 0, this);">No me gusta (<span>{{$value->noMeGusta}}</span>)</a>
					</div>
					{{csrf_field()}}
				</form>

				
					@foreach($value->tpublicacioncomentario as $comentario)
					<div class="box" style="margin-bottom: 4px;">
					
							<div class="box-body">
								<div class="col-md-8">
									<table>
										<tbody>
										<tr>
											<td style="width: 45px;">
												<img src="{{asset('avatar/'.$comentario->codigoUsuario.'.'.$comentario->tusuario->extensionAvatar)}}" height="40" width="40" style="border: 1px solid #999999;border-radius: 50px;">
											</td>
											<td style="font-family: cursive;">
												
												<b>{{$comentario->tusuario->nombre.' '.$comentario->tusuario->apellido}}</b>
												<br>
												{{$comentario->descripcion}}
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="box-footer" style="text-align: right;">
						@if(Session::get('codigoUsuario')==$comentario->codigoUsuario)
							<a href="{{url('publicacion/eliminar/'.$value->codigoPublicacion)}}" style="color: red;">Eliminar</a> | 
						@endif
						|<a href="#" onclick="publicacionComentario('{{ $comentario->codigoPublicacionComentario }}');">Comentar</a>|	
						<span style="color: #999999;font-size: 12px;">{{$comentario->created_at}}</span>
					</div>
					
					</div>
				@endforeach
				

			</div>
		@endforeach
	</div>
</div>
 <div id="divComentario"></div>  
<div id="mymodal"></div>>
@if($tPublicacion!=null)

	<div class="modal modal-primary fade" id="modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Noticacion</h4>
              </div>
              <div class="modal-body">
                <p>{{$tPublicacion->descripcion}}</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline">Save changes</button>
              </div>
            </div>
       
          </div>
     
    </div>

@endif
<script>

$(function()
{
	$( ".divEtiquetadosUsuario" ).click(function() {
 	$('.divEtiquetados').show();
});
	$('.divEtiquetados').hide();

	$('#modal').modal('show');
});

	$(document).on('scroll', function()
	{
		if(($(window).scrollTop()+$(window).height())+5>$(document).height() && !($('#divModalAjax').is(':visible')))
		{
			var lastCreatedAt=$($('#divContenedorPublicaciones').find('.hdCreatedAt')[$('#divContenedorPublicaciones').find('.hdCreatedAt').length-1]).val();

			$('#divModalAjax').show();

			$.ajax(
			{
				url: '{{url('publicacion/vermenorfechacreacion')}}',
				type: 'POST',
				data:
				{
					_token : '{{csrf_token()}}',
					createdAt: lastCreatedAt
				},
				cache: false,
				async: true
			}).done(function(html) 
			{
				$('#divContenedorPublicaciones').append(html);

				$('#divModalAjax').hide();
			}).fail(function()
			{
				swal('Error', 'Error no controlado.', 'error');
			});
		}
	});

	function enviarReaccion(codigoPublicacion, meGusta, element)
	{
		$('#divModalAjax').show();

		$.ajax(
		{
			url: '{{url('publicaciontusuarioevtreaccion/insertar')}}',
			type: 'POST',
			data:
			{
				_token : '{{csrf_token()}}',
				codigoPublicacion: codigoPublicacion,
				gusta: meGusta
			},
			cache: false,
			async: true
		}).done(function(object) 
		{
			$('#divModalAjax').hide();

			if(object.correcto)
			{
				if(typeof object.cambio!='undefined')
				{
					var elementoPadre=$(element).parent();
					var etiquetaModicar=null;

					if(meGusta)
					{
						etiquetaModicar=$(elementoPadre).find('> a')[2];
					}
					else
					{
						etiquetaModicar=$(elementoPadre).find('> a')[1];
					}

					var cantidadActualDos=parseInt($($(etiquetaModicar).find('> span')[0]).text());

					cantidadActualDos--;

					$($(etiquetaModicar).find('> span')[0]).text(cantidadActualDos);
				}

				var cantidadActual=parseInt($($(element).find('span')[0]).text());

				cantidadActual++;

				$($(element).find('span')[0]).text(cantidadActual);
			}
			else
			{
				swal('Error', object.mensajeGlobal, 'error');
			}
		}).fail(function()
		{
			swal('Error', 'Error no controlado.', 'error');
		});
	}
function publicacionComentario(codigoPublicacion)
  {
   $.ajax(
   {
    url:'{{ url('publicacion/comentario/insertar') }}',
    type:'Post',
    data:{ _token :'{{csrf_token()}}',codigoPublicacion:codigoPublicacion},
    cache: false,
    async: true

   }).done(function(resultado)
   {
    $('#divComentario').html(resultado);
    $('#modal-primary').modal('show')
     


   }).fail(function()
   {
    swal('Error', 'Error no controlado.', 'error');
   });

  }

  function etiquetarUsuario(codigoPublicacion)
  {
  	 $.ajax(
   {
    url:'{{ url('publicacion/etiqueta/insertar') }}',
    type:'Post',
    data:{ _token :'{{csrf_token()}}',codigoPublicacion:codigoPublicacion},
    cache: false,
    async: true

   }).done(function(resultado)
   {
    $('#etiqueta').html(resultado);
    $('#modal-primary1').modal('show');
    
     


   }).fail(function()
   {
    swal('Error', 'Error no controlado.', 'error');
   });

  }
  function btnGuardarEtiqueta(codigoUsuario,codigoPublicacion)
{
     $.ajax(
   {
        url:'{{ url('publicacion/etiqueta/insertar1') }}',
        type:'post',
        data:{ _token :'{{csrf_token()}}',codigoUsuario:codigoUsuario,codigoPublicacion:codigoPublicacion},
        //contentType: "application/json;charset=utf-8",
        dataType: "json",
        cache: false,
        async: true

 }).done(function(object)
	{
	if(object.correcto)
	{
		$('#modal-primary1').modal('hide');
		$('#etiqueta').modal('hide');
		swal('alert', object.mensajeGlobal, 'success');
		
	}
	else
	{
	   $('#modal-primary1').modal('hide');
		$('#etiqueta').modal('hide');
		swal('alert', object.mensajeGlobal, 'error');	
	}
     }).fail(function()
        {
           swal('Error', 'Error no controlado.', 'error');
         });

}
 
 $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script> 
</script>
@endsection
