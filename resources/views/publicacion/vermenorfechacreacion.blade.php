@foreach($listaTPublicacion as $key => $value)
	<div class="box" style="margin-bottom: 4px;">
		<form action="{{url('publicacion/index')}}" method="post">
			<div class="box-body">
				<div class="col-md-12">
					<table>
						<tbody>
							<tr>
								<td style="width: 45px;">
									<img src="{{asset('avatar/'.$value->tusuario->codigoUsuario.'.'.$value->tusuario->extensionAvatar)}}" height="40" width="40" style="border: 1px solid #999999;border-radius: 50px;">
								</td>
								<td>
									<input type="hidden" class="hdCreatedAt" value="{{$value->created_at}}">
									<b>{{$value->tusuario->nombre.' '.$value->tusuario->apellido}}</b>
									<br>
									{{$value->descripcion}}
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box-footer" style="text-align: right;">
				@if(Session::get('codigoUsuario')==$value->codigoUsuario)
					<a href="{{url('publicacion/eliminar/'.$value->codigoPublicacion)}}" style="color: red;">Eliminar</a> | 
				@endif
				<span style="color: #999999;font-size: 12px;">{{$value->created_at}}</span> - <a href="#" onclick="enviarReaccion('{{$value->codigoPublicacion}}', 1, this);">Me gusta (<span>{{$value->meGusta}}</span>)</a> | <a href="#" onclick="enviarReaccion('{{$value->codigoPublicacion}}', 0, this);">No me gusta (<span>{{$value->noMeGusta}}</span>)</a>
			</div>
			{{csrf_field()}}
		</form>
	</div>
@endforeach
