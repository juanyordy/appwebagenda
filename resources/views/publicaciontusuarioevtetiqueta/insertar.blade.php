<div class="modal modal-success fade" id="modal-primary1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Etiquetar</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  
            	   <div class="box-body">
                       <div class="form-group">
                            
                             <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                                @foreach($tUsuarios as $usuario)
                                <div class="col-md-4">
                                     <li>
                                    <img src="{{asset('avatar/'.$usuario->codigoUsuario.'.'.$usuario->extensionAvatar)}}" height="40" width="40" style="border: 1px solid #999999;border-radius: 50px;">
                                    <a class="users-list-name" onclick="btnGuardarEtiqueta('{{$usuario->codigoUsuario}}','{{ $codigoPublicacion }}')" href="#">{{$usuario->nombre.' '.$usuario->apellido}}</a>
                                </li>
                               
                                </div>
                                 @endforeach
                               
                            </ul>
                        </div>
                            
			             </div>

        	        </div>
		         
                    
                </div>    
            </div>
            <div class="modal-footer">
              
            </div>
        </div>
    </div>
</div>

<script>

</script>