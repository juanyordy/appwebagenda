<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Spring Valley Software</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
	<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css')}}">	
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">

	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/_all-skins.min.css')}}">
	<!-- Morris chart -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
	<!-- jvectormap -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/jvectormap/jquery-jvectormap.css')}}">
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<script src="{{ asset('plugin/sweetalert/sweetalert.min.js') }}"></script>		
	<script src="{{ asset('plugin/sweetalert/core-js.js') }}"></script>	
	<link rel="stylesheet" href="{{asset('modal/modalajax.css')}}">
	<link rel="stylesheet" href="{{asset('plugin/formvalidation/formValidation.min.css')}}">

	<script src="{{asset('plugin/sweetalert/sweetalert.min.js')}}"></script>

	<!-- jQuery 3 -->
	<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{{asset('adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

	<header class="main-header">
	<!-- Logo -->
	<a href="index2.html" class="logo">
	<!-- mini logo for sidebar mini 50x50 pixels -->
	<span class="logo-mini">POST</span>
	<!-- logo for regular state and mobile devices -->
	<span class="logo-lg"><b>Spring</b>VS</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
	<span class="sr-only">Toggle navigation</span>
	</a>

	<div class="navbar-custom-menu"  >
	<ul class="nav navbar-nav">
	<!-- Notifications: style can be found in dropdown.less -->
	<li class="dropdown notifications-menu" style="border-left: 1px solid #4094c4;" onclick="notificaciones();">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-bell-o"></i>
		<span class="label label-warning" >{{ $cantidadNotificacion }}</span>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Tienes {{ $cantidadNotificacion }} notificaciones </li>
			<li>
				<!-- inner menu: contains the actual data -->
				<ul id="contenedorLiNotificacionGeneral" class="menu">					
					
				</ul>
			</li>
			<li class="footer"><a href="#">View all</a></li>
		</ul>
	</li>
	<!-- User Account: style can be found in dropdown.less -->
	<li class="dropdown user user-menu" style="border-left: 1px solid #4094c4;">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	<img src="{{asset('avatar/'.Session::get('codigoUsuario').'.'.Session::get('extensionAvatar'))}}" class="user-image" alt="User Image">
	<span class="hidden-xs">{{Session::get('correoElectronico')}}</span>
	</a>
	<ul class="dropdown-menu">
	<!-- User image -->
	<li class="user-header">
	<img src="{{asset('avatar/'.Session::get('codigoUsuario').'.'.Session::get('extensionAvatar'))}}" class="img-circle" alt="User Image">

	<p>
	{{Session::get('nombre')}}
	<small>Usuario verificado</small>
	</p>
	</li>
	<!-- Menu Footer-->
	<li class="user-footer">
	<div class="pull-left">
	<a href="{{url('usuario/editar')}}" class="btn btn-default btn-flat">Perfil</a>
	</div>
	<div class="pull-right">
	<a href="{{url('usuario/logout')}}" class="btn btn-default btn-flat">Salir</a>
	</div>
	</li>
	</ul>
	</li>
	<!-- Control Sidebar Toggle Button -->
	<li style="border-left: 1px solid #cccccc;">
		<a href="{{url('usuario/logout')}}"><i class="fa fa-sign-out"></i></a>
	</li>
	</ul>
	</div>
	</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
	<div class="pull-left image">
	<img src="{{asset('avatar/'.Session::get('codigoUsuario').'.'.Session::get('extensionAvatar'))}}" class="img-circle" alt="User Image">
	</div>
	<div class="pull-left info">
	<p>{{Session::get('nombre')}}</p>
	<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	</div>
	</div>
	<!-- search form -->
	<form action="#" method="get" class="sidebar-form">
	<div class="input-group">
	<input type="text" name="q" class="form-control" placeholder="Buscar...">
	<span class="input-group-btn">
	<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
	</button>
	</span>
	</div>
	</form>
	<!-- /.search form -->
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu" data-widget="tree">
	<li class="header">NAVEGACIÓN EN EL SISTEMA</li>
	<li class="active treeview">
	<a href="#">
	<i class="fa fa-bars"></i> <span>Menú principal</span>
	<span class="pull-right-container">
	<i class="fa fa-angle-left pull-right"></i>
	</span>
	</a>
	<ul class="treeview-menu">
		<li class="active"><a href="{{url('/')}}"><i class="fa fa-home"></i>Inicio</a></li>
		<li class="active"><a href="{{url('/usuario/editar')}}"><i class="fa fa-user"></i>Mis datos personales</a></li>
		<li class="active"><a href="{{url('/publicacion/index')}}"><i class="fa fa-comment"></i>Publicaciones</a></li>
	</ul>
	</li>
	</ul>
	</section>
	<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<h1>
	@yield('tituloGeneral')
	<small>@yield('subTituloGeneral')</small>
	</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		@if(Session::has('mensajeGlobal'))
			<script>
				swal('{{Session::get('correcto') ? 'Correcto' : 'Error en los datos'}}', '{!!Session::get('mensajeGlobal')!!}', '{{Session::get('correcto') ? 'success' : 'error'}}');
			</script>
		@endif
		<!-- Main row -->
		<div class="row">
			<div id="divModalAjax"><span>Cargando</span></div>
			<div id="contenedorGeneral"></div>
			@yield('cuerpoGeneral')
		</div>
		<!-- /.row (main row) -->
	</section>
	<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
	<div class="pull-right hidden-xs">
	<b>Version</b> 1.0.0
	</div>
	<strong>Copyright &copy; 2017-{{date('Y')}} <a href="http://codideep.com">Spring Valley Software</a>.</strong> Todos los derechos reservados.
	</footer>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
	immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
	<!-- Morris.js charts -->
	<script src="{{asset('adminlte/bower_components/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('adminlte/bower_components/morris.js/morris.min.js')}}"></script>
	<!-- Sparkline -->
	<script src="{{asset('adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
	<!-- jvectormap -->
	<script src="{{asset('adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script src="{{asset('adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{asset('adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
	<!-- daterangepicker -->
	<script src="{{asset('adminlte/bower_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<!-- datepicker -->
	<script src="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
	<!-- Slimscroll -->
	<script src="{{asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<!-- FastClick -->
	<script src="{{asset('adminlte/bower_components/fastclick/lib/fastclick.js')}}"></script>
	<!-- AdminLTE App -->
	<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
	<script src="{{asset('plugin/formvalidation/formValidation.min.js')}}"></script>
	<script src="{{asset('plugin/formvalidation/bootstrap.validation.min.js')}}"></script>

	<script>
		$(function()
		{
			$('input').attr('autocomplete', 'off');
		});
		function notificaciones()
{
	$('#divModalAjax').show();

			$.ajax(
			{
				url: '{{url('notificacion/nueva')}}',
				type: 'POST',
				data:
				{
					_token : '{{csrf_token()}}',
					//createdAt: lastCreatedAt
				},
				cache: false,
				async: true
			}).done(function(objectJSON) 
			{
				var html='';

				for(var i=0; i<objectJSON.length; i++)
				{
					html+='<li>'
						+'<a href="'+objectJSON[i].url+'">'
							+'<i class="fa fa-users text-aqua"></i>'+objectJSON[i].descripcion
						+'</a>'
					+'</li>';
				}

				$('#contenedorLiNotificacionGeneral').html(html);

				$('#divModalAjax').hide();

			}).fail(function()
			{
				swal('Error', 'Error no controlado.', 'error');
			});	
}	
	</script>
</body>
</html>