<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Cambiar avatar</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="frmCambiarAvatar" action="{{url('usuario/cambiaravatar')}}" method="post" enctype="multipart/form-data">
						<div class="col-md-3">
							<label for="fileAvatar">Seleccionar avatar</label>
						</div>
						<div class="col-md-9">
							<input type="file" id="fileAvatar" name="fileAvatar">
						</div>
						<input type="hidden" name="hdPost">
						{{csrf_field()}}
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-info" onclick="enviarFrmCambiarAvatar();">Subir avatar</button>
			</div>
		</div>
	</div>
</div>
<script>
	function enviarFrmCambiarAvatar()
	{
		$('#frmCambiarAvatar').submit();
	}
</script>