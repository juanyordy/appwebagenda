<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Agenda SVS</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('adminlte/plugins/iCheck/square/blue.css')}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="{{asset('plugin/sweetalert/sweetalert.css')}}">

<script src="{{asset('plugin/sweetalert/sweetalert.min.js')}}"></script>
</head>
<body class="hold-transition register-page">
	@if(Session::has('mensajeGlobal'))
		<script>
			swal('{{Session::get('correcto')=='Si' ? 'Correcto' : 'Error en los datos'}}', '{!!Session::get('mensajeGlobal')!!}', '{{Session::get('correcto')=='Si' ? 'success' : 'error'}}');
		</script>
	@endif
	<div class="register-box">
	<div class="register-logo">
	<b>Sistema de agenda</b> SVS
	</div>

	<div class="register-box-body">
	<p class="login-box-msg">Regístrate en este sistema</p>

	<form id="frmInsertarUsuario" action="{{url('usuario/insertar')}}" method="post">
		<div class="form-group has-feedback">
			<input type="text" id="txtNombre" name="txtNombre" class="form-control" placeholder="Nombre" value="{{old('txtNombre')}}" autocomplete="off">
			<span class="glyphicon glyphicon-user form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" id="txtApellido" name="txtApellido" class="form-control" placeholder="Apellido" value="{{old('txtApellido')}}" autocomplete="off">
			<span class="glyphicon glyphicon-user form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" id="txtCorreoElectronico" name="txtCorreoElectronico" class="form-control" placeholder="Correo electrónico" value="{{old('txtCorreoElectronico')}}" autocomplete="off">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" id="passContrasenia" name="passContrasenia" class="form-control" placeholder="Contraseña">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" id="passContraseniaRepita" name="passContraseniaRepita" class="form-control" placeholder="Repita contraseña">
			<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-7">
				<a href="{{url('usuario/login')}}">Ir al LogIn</a>
			</div>
			<!-- /.col -->
			<div class="col-xs-5">
				<input type="button" class="btn btn-info btn-block btn-flat" value="Registrarme" onclick="enviarFrmInsertarUsuario();">
			</div>
			<!-- /.col -->
		</div>
		{{csrf_field()}}
	</form>
	</div>
	<!-- /.form-box -->
	</div>
	<!-- /.register-box -->

	<!-- jQuery 3 -->
	<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

	<script>
		function enviarFrmInsertarUsuario()
		{
			swal(
			{
				title: 'Confirmar operación',
				text: "Está seguro(a) de regitrar esta información?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Confirmar'
			}).then(function()
			{
				$('#frmInsertarUsuario').submit();
			});
		}
	</script>
</body>
</html>