@extends('template.templategeneric')
@section('tituloGeneral', 'Usuario')
@section('subTituloGeneral', 'Mis datos personales')
@section('cuerpoGeneral')
<div class="col-md-2">
	<div style="text-align: center;">
		<div>
			<img src="{{asset('avatar/'.Session::get('codigoUsuario').'.'.Session::get('extensionAvatar'))}}?x={{date('Y-m-d H:i:s')}}" height="160" width="160">
		</div>
		<div>
			<button type="button" class="btn btn-success" onclick="mostrarModalCambiarAvatar();" style="margin-top: 7px;">Cambiar avatar</button>
		</div>
	</div>
</div>
<div class="col-md-10">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Datos de usuarios</h3>
		</div>
		<form id="frmEditarUsuario" action="{{url('usuario/editar')}}" method="post">
			<div class="box-body">
				<div class="form-group">
					<label for="txtNombre">Nombre</label>
					<input type="text" id="txtNombre" name="txtNombre" class="form-control" placeholder="Obligatorio" value="{{$tUsuario->nombre}}">
				</div>
				<div class="form-group">
					<label for="txtApellido">Apellido</label>
					<input type="text" id="txtApellido" name="txtApellido" class="form-control" placeholder="Obligatorio" value="{{$tUsuario->apellido}}">
				</div>
				<div class="form-group">
					<label for="txtCorreoElectronico">Correo electrónico</label>
					<input type="text" id="txtCorreoElectronico" name="txtCorreoElectronico" class="form-control" placeholder="Obligatorio" value="{{$tUsuario->correoElectronico}}">
				</div>
				<div class="form-group">
					<label for="txtDni">Dni</label>
					<input type="text" id="txtDni" name="txtDni" class="form-control" value="{{$tUsuario->dni}}">
				</div>
				<div class="form-group">
					<label for="dateFechaNacimiento">Fecha de nacimiento</label>
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" id="dateFechaNacimiento" name="dateFechaNacimiento" class="form-control pull-right" placeholder="Obligatorio" value="{{$tUsuario->fechaNacimiento}}">
					</div>
				</div>
				<div class="form-group">
					<label>Sexo</label>
					<div>
						<label for="radioSexoM"><input type="radio" id="radioSexoM" name="radioSexo" value="M" {{$tUsuario->sexo ? 'checked="true"' : ''}}> Masculino</label>
						&nbsp;&nbsp;
						<label for="radioSexoF"><input type="radio" id="radioSexoF" name="radioSexo" value="F" {{!($tUsuario->sexo) ? 'checked="true"' : ''}}> Femenino</label>
					</div>
				</div>
			</div>
			<div class="box-footer" style="text-align: right;">
				<button type="button" class="btn btn-info" onclick="enviarFrmEditarUsuario();">Guardar cambios</button>
			</div>
			{{csrf_field()}}
		</form>
	</div>
</div>
<script>
	$(function()
	{
		$('#dateFechaNacimiento').datepicker(
		{
			format: 'yyyy-mm-dd',
			autoclose: true
		});
	});

	function mostrarModalCambiarAvatar()
	{
		$.ajax(
		{
			url: '{{url('usuario/cambiaravatar')}}',
			type: 'POST',
			data: { _token : '{{csrf_token()}}' },
			cache: false,
			async: true
		}).done(function(pagina) 
		{
			$('#contenedorGeneral').html(pagina);

			$('#modal-default').modal();
		}).fail(function()
		{
			swal('Error', 'Error no controlado.', 'error');
		});
	}

	function enviarFrmEditarUsuario()
	{
		swal(
		{
			title: 'Confirmar operación',
			text: "Realmente desea guardar los cambios?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Confirmar'
		}).then(function()
		{
			$('#frmEditarUsuario').submit();
		});
	}
</script>
@endsection